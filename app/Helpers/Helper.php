<?php

function getStatus ($status) {
    if ($status == 1) {
        return "draft";
    } elseif ($status == 2) {
        if (Auth::user()->role == "people")
            return "sent";
        else
            return "received";
    } elseif ($status == 3) {
        return "responded";
    }
}

function getColor ($status) {
    if ($status == 1) {
        return "warning";
    } elseif ($status == 2) {
        return "info";
    } elseif ($status == 3) {
        return "success";
    }
}

function getIcon ($status) {
    if ($status == 1) {
        return "drafts";
    } elseif ($status == 2) {
        if (Auth::user()->role == "people")
            return "near_me";
        else
            return "inbox";
    } elseif ($status == 3) {
        return "done_all";
    }
}

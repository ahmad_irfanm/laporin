<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
    //
    protected $guarded = [];

    public function officer () {
        return $this->belongsTo('App\Officer');
    }
}

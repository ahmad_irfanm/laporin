<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Complaint extends Model
{
    //
    protected $guarded = [];

    public function people () {
        return $this->belongsTo('App\People');
    }

    public function response () {
        return $this->hasOne('App\Response');
    }
}

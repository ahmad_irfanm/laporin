<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class People extends Model
{
    //
    protected $guarded = [];

    public function user () {
        return $this->belongsTo('App\User');
    }

    public function complaints () {
        return $this->hasMany('App\Complaint');
    }
}


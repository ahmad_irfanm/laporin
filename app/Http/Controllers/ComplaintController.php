<?php

namespace App\Http\Controllers;

use App\Complaint;
use Illuminate\Http\Request;
use Validator;
use Auth;
use App\Response;

class ComplaintController extends Controller
{
    public function __construct(Complaint $complaint, Response $response)
    {
        $this->complaint = $complaint;
        $this->response = $response;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role == "people") {
            $complaints = $this->complaint->wherePeopleId(Auth::user()->people->id)->orderBy('created_at', 'desc')->get();
        } else {
            $complaints = $this->complaint->orderBy('created_at', 'desc')->whereIn('status', [2, 3])->get();
        }
        //
        return view('complaint.index', [
            'complaints' => $complaints
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('complaint.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validation = Validator::make($request->all(), [
            'title' => 'required',
            'proof' => 'required|mimes:png,jpg,jpeg'
        ]);

        // validation fails
        if ($validation->fails())
            return redirect()->back()->withErrors($validation)->withInput($request->all());

        // upload proof
        $proof = $request->file('proof')->store('public/proofs');

        // validation success
        $complaint = $this->complaint->create([
            'people_id' => Auth::user()->people->id,
            'title' => $request->title,
            'description' => $request->description,
            'proof' => $proof,
            'location' => $request->location
        ]);

        // if success
        if ($complaint) {
            return redirect()->route('people.complaint.show', ['complaint' => $complaint->id])->with('msg', 'Complaint created successful');
        } else {
            return redirect()->back()->withErrors(['title' => 'Something errors'])->withInput($request->all());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Complaint  $complaint
     * @return \Illuminate\Http\Response
     */
    public function show(Complaint $complaint)
    {
        return view('complaint.show', [
            'complaint' => $complaint
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Complaint  $complaint
     * @return \Illuminate\Http\Response
     */
    public function edit(Complaint $complaint)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Complaint  $complaint
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Complaint $complaint)
    {

        if ($request->status == 3) {
            $validation = Validator::make($request->all(), [
                'content' => 'required'
            ]);

            // validation fails
            if ($validation->fails()) {
                return redirect()->back()->withErrors($validation)->withInput($request->all());
            }
        }

        // validation success
        $update = $complaint->update([
            'status' => $request->status
        ]);

        if ($request->status == 3) {
            $this->response->create([
                'complaint_id' => $complaint->id,
                'officer_id' => Auth::user()->officer->id,
                'content' => $request->content
            ]);
        }

        // if success
        if ($update) {
            return redirect()->route(Auth::user()->role . '.complaint.show', ['complaint' => $complaint->id])->with('msg', 'Complaint updated successful');
        } else {
            return redirect()->back()->withErrors(['title' => 'Something errors'])->withInput($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Complaint  $complaint
     * @return \Illuminate\Http\Response
     */
    public function destroy(Complaint $complaint)
    {
        //
    }
}

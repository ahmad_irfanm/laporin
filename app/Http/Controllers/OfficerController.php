<?php

namespace App\Http\Controllers;

use App\Officer;
use App\User;
use Illuminate\Http\Request;
use Validator;
use Auth;

class OfficerController extends Controller
{
    public function __construct(Officer $officer, User $user)
    {
        $this->officer = $officer;
        $this->user = $user;
    }

    public function dashboard () {
        return view('officer.dashboard');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $officers = $this->user->where('role', 'officer')->orderBy('username', 'asc')->get()->map(function ($user) {
            return $user->officer;
        });

        //
        return view('officer.index', [
            'officers' => $officers
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'username' => 'required|unique:users,username',
            'name' => 'required',
            'phone' => 'required',
            'photo' => 'mimes:jpg,png,jpeg',
            'password' => 'required|confirmed'
        ]);

        // validation fails
        if ($validation->fails())
            return redirect()->back()->withErrors($validation)->withInput($request->all());

        // upload proof
        if ($request->file('photo')) {
            $photo = $request->file('photo')->store('public/officers');
        } else {
            $photo = 'public/officers/default.jpg';
        }

        // validation success
        $user = $this->user->create([
            'username' => $request->username,
            'password' => bcrypt($request->password),
            'role' => 'officer'
        ]);;
        $officer = $this->officer->create([
            'name' => $request->name,
            'phone' => $request->phone,
            'photo' => $photo,
            'user_id' => $user->id
        ]);

        // if success
        if ($officer) {
            return redirect()->back()->with('msg', 'Officer created successful');
        } else {
            return redirect()->back()->withErrors(['title' => 'Something errors'])->withInput($request->all());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Officer  $officer
     * @return \Illuminate\Http\Response
     */
    public function show(Officer $officer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Officer  $officer
     * @return \Illuminate\Http\Response
     */
    public function edit(Officer $officer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Officer  $officer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Officer $officer)
    {
        //
        //
        $validation = Validator::make($request->all(), [
            'username' => 'required|unique:users,username,' . $officer->user->id,
            'name' => 'required',
            'phone' => 'required',
            'photo' => 'mimes:jpg,png,jpeg'
        ]);

        // validation fails
        if ($validation->fails())
            return redirect()->back()->withErrors($validation)->withInput($request->all());

        // upload proof
        if ($request->file('photo')) {
            $photo = $request->file('photo')->store('public/officers');
        } else {
            $photo = $officer->photo;
        }

        // validation success
        $update = $officer->update([
            'name' => $request->name,
            'phone' => $request->phone,
            'photo' => $photo
        ]);
        $officer->user()->update([
            'username' => $request->username
        ]);

        // if success
        if ($update) {
            return redirect()->back()->with('msg', 'Officer updated successful');
        } else {
            return redirect()->back()->withErrors(['title' => 'Something errors'])->withInput($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Officer  $officer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Officer $officer)
    {
        //
        if ($officer->responses->count() > 0) {
            return response()->json(['message' => 'Officer have complaints are response'], 401);
        }

        $officer->user()->delete();

        return response()->json(['message' => 'Officer deleted successful'], 200);
    }
}

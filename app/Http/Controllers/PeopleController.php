<?php

namespace App\Http\Controllers;

use App\People;
use Illuminate\Http\Request;
use Validator;
use Auth;
use App\User;

class PeopleController extends Controller
{
    public function __construct(People $people, User $user)
    {
        $this->people = $people;
        $this->user = $user;
    }

    public function dashboard () {
        return view('people.dashboard');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $peoples = $this->people->orderBy('name', 'asc')->get();

        //
        return view('people.index', [
            'peoples' => $peoples
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'username' => 'required|unique:users,username',
            'name' => 'required',
            'nik' => 'required',
            'phone' => 'required',
            'photo' => 'mimes:jpg,png,jpeg',
            'password' => 'required|confirmed'
        ]);

        // validation fails
        if ($validation->fails())
            return redirect()->back()->withErrors($validation)->withInput($request->all());

        // upload proof
        if ($request->file('photo')) {
            $photo = $request->file('photo')->store('public/users');
        } else {
            $photo = 'public/users/default.jpg';
        }

        // validation success
        $user = $this->user->create([
            'username' => $request->username,
            'password' => bcrypt($request->password),
            'role' => 'people'
        ]);;
        $people = $this->people->create([
            'name' => $request->name,
            'nik' => $request->nik,
            'address' => $request->address,
            'phone' => $request->phone,
            'photo' => $photo,
            'user_id' => $user->id
        ]);

        // if success
        if ($people) {
            return redirect()->back()->with('msg', 'People created successful');
        } else {
            return redirect()->back()->withErrors(['title' => 'Something errors'])->withInput($request->all());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Officer  $officer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $people)
    {
        $people = People::find($people);

        //
        $validation = Validator::make($request->all(), [
            'username' => 'required|unique:users,username,' . $people->user->id,
            'name' => 'required',
            'nik' => 'required',
            'phone' => 'required',
            'photo' => 'mimes:jpg,png,jpeg'
        ]);

        // validation fails
        if ($validation->fails())
            return redirect()->back()->withErrors($validation)->withInput($request->all());

        // upload proof
        if ($request->file('photo')) {
            $photo = $request->file('photo')->store('public/users');
        } else {
            $photo = $people->photo;
        }

        // validation success
        $update = $people->update([
            'name' => $request->name,
            'phone' => $request->phone,
            'photo' => $photo
        ]);
        $people->user()->update([
            'username' => $request->username
        ]);

        // if success
        if ($update) {
            return redirect()->back()->with('msg', 'People updated successful');
        } else {
            return redirect()->back()->withErrors(['title' => 'Something errors'])->withInput($request->all());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Officer  $officer
     * @return \Illuminate\Http\Response
     */
    public function destroy($people)
    {
        $people = People::find($people);
        //
        if ($people->complaints->count() > 0) {
            return response()->json(['message' => 'People have some complaints'], 401);
        }

        $people->user()->delete();

        return response()->json(['message' => 'People deleted successful'], 200);
    }
}

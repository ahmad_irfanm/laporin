@extends('layouts.admin')

@section('title')
    Dashboard
@endsection

@section('dashboard')
    active
@endsection

@section('css')
@endsection

@section('js')
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title">
                <span class="page-title-icon bg-gradient-primary text-white mr-2">
                  <i class="mdi">home</i>
                </span> Dashboard </h3>
            <nav aria-label="breadcrumb">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item active" aria-current="page">
                        <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="row">
            <div class="col-md-4 stretch-card grid-margin">
                <div class="card bg-gradient-danger card-img-holder text-white">
                    <div class="card-body">
                        <img src="{{asset('assets/images/dashboard/circle.svg')}}" class="card-img-absolute" alt="circle-image">
                        <h4 class="font-weight-normal mb-3">Complaints <i class="mdi mdi-24px float-right">comment</i>
                        </h4>
                        <h2 class="mb-5">{{\App\Complaint::where('status', '!=', 1)->count()}}</h2>
                        <h6 class="card-text">All of complaints</h6>
                    </div>
                </div>
            </div>
            <div class="col-md-4 stretch-card grid-margin">
                <div class="card bg-gradient-info card-img-holder text-white">
                    <div class="card-body">
                        <img src="{{asset('assets/images/dashboard/circle.svg')}}" class="card-img-absolute" alt="circle-image">
                        <h4 class="font-weight-normal mb-3">People <i class="mdi mdi-24px float-right">face</i>
                        </h4>
                        <h2 class="mb-5">{{\App\People::count()}}</h2>
                        <h6 class="card-text">All of people</h6>
                    </div>
                </div>
            </div>
            <div class="col-md-4 stretch-card grid-margin">
                <div class="card bg-gradient-success card-img-holder text-white">
                    <div class="card-body">
                        <img src="{{asset('assets/images/dashboard/circle.svg')}}" class="card-img-absolute" alt="circle-image">
                        <h4 class="font-weight-normal mb-3">Officer <i class="mdi mdi-24px float-right">assignment_ind</i>
                        </h4>
                        <h2 class="mb-5">{{\App\Officer::count()}}</h2>
                        <h6 class="card-text">All of officer</h6>
                    </div>
                </div>
            </div>

            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 grid-margin stretch-card">
                <div class="card review-card">
                    <div class="card-header header-sm d-flex justify-content-between align-items-center">
                        <h4 class="card-title mb-0">New received complaints</h4>
                        <div class="wrapper d-flex align-items-center">
                            <div class="dropdown">
                                <button class="btn btn-transparent icon-btn pr-0" type="button" id="dropdownMenuIconButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="mdi">more_vert</i>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton1" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 46px, 0px);">
                                    <a class="dropdown-item" href="{{route('people.complaint.index')}}">Today</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body no-gutter">

                        @foreach(\App\Complaint::where('status', '2')->orderBy('created_at', 'desc')->skip(0)->take(5)->get() as $complaint)
                        <div class="list-item">
                            <div class="preview-image">
                                <img class="img-sm rounded-circle" src="{{URL::asset('storage/app/' . $complaint->people->photo)}}" alt="profile image">
                            </div>
                            <div class="content">
                                <div class="d-flex align-items-center">
                                    <h6 class="product-name">{{$complaint->people->name}}</h6>
                                    <small class="time ml-3 d-none d-sm-block">{{$complaint->created_at->diffForHumans()}}</small>
                                    <div class="ml-auto">
                                        <a href="{{route(Auth::user()->role . '.complaint.show', ['complaint' => $complaint->id])}}" class="btn btn-primary btn-sm" style="display: inline;">View complaint</a>
                                    </div>
                                </div>
                                <div class="d-flex align-items-center">
                                    <p class="user-name">Say :</p>
                                    <p class="review-text d-block">{{$complaint->title}}</p>
                                </div>
                            </div>
                        </div>
                        @endforeach

                        @if(\App\Complaint::where('status', '2')->orderBy('created_at', 'desc')->skip(0)->take(5)->count() == 0)
                            No data
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

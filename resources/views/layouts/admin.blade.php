<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from www.bootstrapdash.com/demo/purple/jquery/template/demo_1/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 09 Feb 2020 16:02:14 GMT -->
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <title>{{config('app.name', 'Laravel')}} | @yield('title')</title>
    <!-- plugins:css -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Merriweather:700|Montserrat:400,500,700&display=swap" rel="stylesheet">
    <style>
        @font-face {
            font-family: 'Material Icons';
            font-style: normal;
            font-weight: 400;
            src: url(https://example.com/MaterialIcons-Regular.eot); /* For IE6-8 */
            src: local('Material Icons'),
            local('MaterialIcons-Regular'),
            url(https://example.com/MaterialIcons-Regular.woff2) format('woff2'),
            url(https://example.com/MaterialIcons-Regular.woff) format('woff'),
            url(https://example.com/MaterialIcons-Regular.ttf) format('truetype');
        }
        .mdi {
            font-family: 'Material Icons';
            font-weight: normal;
            font-style: normal;
            font-size: 24px;  /* Preferred icon size */
            display: inline-block;
            line-height: 1;
            text-transform: none;
            letter-spacing: normal;
            word-wrap: normal;
            white-space: nowrap;
            direction: ltr;

            /* Support for all WebKit browsers. */
            -webkit-font-smoothing: antialiased;
            /* Support for Safari and Chrome. */
            text-rendering: optimizeLegibility;

            /* Support for Firefox. */
            -moz-osx-font-smoothing: grayscale;

            /* Support for IE. */
            font-feature-settings: 'liga';
        }
        .dropdown .dropdown-toggle:after {
            font: normal normal normal 24px/1 'Material Icons' !important;
            content: "keyboard_arrow_down" !important;
        }
        img {
            object-fit: cover;
        }
    </style>
    <link rel="stylesheet" href="{{asset('assets/vendors/css/vendor.bundle.base.css')}}">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <link rel="stylesheet" href="{{asset('assets/vendors/font-awesome/css/font-awesome.min.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css')}}">
    @yield('css')
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="{{asset('assets/css/demo_1/style.css')}}">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="{{asset('assets/images/logo-mini.png')}}" />
    <style>
        * {
            font-family: 'Montserrat', sans-serif;
        }
        h1, h2, h3, h4, h5, h6, .font-heading {
            font-family: 'Merriweather', serif;
        }
    </style>
</head>
<body class="sidebar-fixed">
<div class="container-scroller">
    <!-- partial:partials/_navbar.html -->
    <nav class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
        <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
            <a class="navbar-brand brand-logo" href="index-2.html"><img src="{{asset('assets/images/logo.png')}}" alt="logo" /></a>
            <a class="navbar-brand brand-logo-mini" href="index-2.html"><img src="{{asset('assets/images/logo-mini.png')}}" alt="logo" /></a>
        </div>
        <div class="navbar-menu-wrapper d-flex align-items-stretch">
            <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
                <span class="mdi">menu</span>
            </button>
    {{--            <div class="search-field d-none d-md-block">--}}
    {{--                <form class="d-flex align-items-center h-100" action="#">--}}
    {{--                    <div class="input-group">--}}
    {{--                        <div class="input-group-prepend bg-transparent">--}}
    {{--                            <i class="input-group-text border-0 mdi">search</i>--}}
    {{--                        </div>--}}
    {{--                        <input type="text" class="form-control bg-transparent border-0" placeholder="Search projects">--}}
    {{--                    </div>--}}
    {{--                </form>--}}
    {{--            </div>--}}
            <ul class="navbar-nav navbar-nav-right">

                <li class="nav-item nav-logout d-none d-lg-block">
                    <a class="nav-link" href="#" onclick="event.preventDefault(); document.getElementById('form-logout').submit()">
                        <i class="mdi mr-2">power_settings_new</i> Sign out
                    </a>
                    <form action="{{route('logout')}}" id="form-logout" method="post">
                        @csrf
                    </form>
                </li>
            </ul>
            <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
                <span class="mdi">menu</span>
            </button>
        </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
        <!-- partial -->
        <!-- partial:partials/_sidebar.html -->
        <nav class="sidebar sidebar-offcanvas" id="sidebar">
            <ul class="nav">
                <li class="nav-item nav-profile">
                    <a href="#" class="nav-link">
                        <div class="nav-profile-image">
                            @if(Auth::user()->role == "people")
                            <img src="{{URL::asset('storage/app/' . (Auth::user()->people->photo))}}" alt="profile">
                            @else
                            <img src="{{URL::asset('storage/app/' . (Auth::user()->officer->photo))}}" alt="profile">
                            @endif
                            <span class="login-status online"></span>
                            <!--change to offline or busy as needed-->
                        </div>
                        <div class="nav-profile-text d-flex flex-column">
                            <span class="font-weight-bold mb-2">
                                @if(Auth::user()->role == "people")
                                    {{Auth::user()->people->name}}
                                @else
                                    {{Auth::user()->officer->name}}
                                @endif
                            </span>
                            <span class="text-secondary text-small">{{Auth::user()->role}}</span>
                        </div>
                    </a>
                </li>

                @if(Auth::user()->role == "admin")
                <li class="nav-item">
                    <a class="nav-link @yield('dashboard')" href="{{route('admin.dashboard')}}">
                        <span class="menu-title">Dashboard</span>
                        <i class="mdi menu-icon">home</i>
                    </a>
                </li>

                <li class="nav-item @yield('complaints')">
                    <a class="nav-link" href="{{route('admin.complaint.index')}}">
                        <span class="menu-title">Complaints</span>
                        <i class="mdi menu-icon">comment</i>
                    </a>
                </li>

                <li class="nav-item @yield('complaints')">
                    <a class="nav-link" href="{{route('admin.officer.index')}}">
                        <span class="menu-title">Officer</span>
                        <i class="mdi menu-icon">assignment_ind</i>
                    </a>
                </li>

                <li class="nav-item @yield('complaints')">
                    <a class="nav-link" href="{{route('admin.people.index')}}">
                        <span class="menu-title">People</span>
                        <i class="mdi menu-icon">face</i>
                    </a>
                </li>

                <li class="nav-item @yield('complaints')">
                    <a class="nav-link" href="{{route('admin.report')}}">
                        <span class="menu-title">Report</span>
                        <i class="mdi menu-icon">local_printshop</i>
                    </a>
                </li>
                @elseif(Auth::user()->role == "officer")
                    <li class="nav-item">
                        <a class="nav-link @yield('dashboard')" href="{{route('officer.dashboard')}}">
                            <span class="menu-title">Dashboard</span>
                            <i class="mdi menu-icon">home</i>
                        </a>
                    </li>

                    <li class="nav-item @yield('complaints')">
                        <a class="nav-link" href="{{route('officer.complaint.index')}}">
                            <span class="menu-title">Complaints</span>
                            <i class="mdi menu-icon">comment</i>
                        </a>
                    </li>
                @else
                <li class="nav-item @yield('dashboard')">
                    <a class="nav-link" href="{{route('people.dashboard')}}">
                        <span class="menu-title">Dashboard</span>
                        <i class="mdi menu-icon">home</i>
                    </a>
                </li>

                <li class="nav-item sidebar-actions">
                  <span class="nav-link">
                    <div class="border-bottom">
                      <h6 class="font-weight-normal mb-3">Complaints</h6>
                    </div>
                    <a href="{{route('people.complaint.create')}}" class="btn btn-block btn-lg btn-gradient-primary mt-4">+ Add a complaint</a>
                  </span>
                </li>

                <li class="nav-item @yield('complaints')">
                    <a class="nav-link" href="{{route('people.complaint.index')}}">
                        <span class="menu-title">Manage Complaints</span>
                        <i class="mdi menu-icon">comment</i>
                    </a>
                </li>
                @endif

            </ul>
        </nav>
        <!-- partial -->
        <div class="main-panel">

            @yield('content')

            <!-- content-wrapper ends -->
            <!-- partial:partials/_footer.html -->
            <footer class="footer">
                <div class="d-sm-flex justify-content-center justify-content-sm-between">
                    <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2020 All rights reserved.</span>
                    <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi text-danger">favorite</i></span>
                </div>
            </footer>
            <!-- partial -->
        </div>
        <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->
<!-- plugins:js -->
<script src="{{asset('assets/vendors/js/vendor.bundle.base.js')}}"></script>
<!-- endinject -->
<!-- Plugin js for this page -->
<script src="{{asset('assets/vendors/chart.js/Chart.min.js')}}"></script>
<script src="{{asset('assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
@yield('js')
<!-- End plugin js for this page -->
<!-- inject:js -->
<script src="{{asset('assets/js/off-canvas.js')}}"></script>
<script src="{{asset('assets/js/hoverable-collapse.js')}}"></script>
<script src="{{asset('assets/js/misc.js')}}"></script>
<script src="{{asset('assets/js/settings.js')}}"></script>
<script src="{{asset('assets/js/todolist.js')}}"></script>
<!-- endinject -->
<!-- Custom js for this page -->
<script src="{{asset('assets/js/dashboard.js')}}"></script>
<!-- End custom js for this page -->
</body>

</html>

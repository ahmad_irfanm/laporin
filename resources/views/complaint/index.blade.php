@extends('layouts.admin')

@section('title')
    @if(Auth::user()->role == "people")
        Manage Complaints
    @else
        List complaints
    @endif
@endsection

@section('complaints')
    active
@endsection

@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendors/datatables.net-bs4/dataTables.bootstrap4.css')}}">
@endsection

@section('js')
    <script src="{{asset('assets/vendors/datatables.net/jquery.dataTables.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-bs4/dataTables.bootstrap4.js')}}"></script>
    <script src="{{asset('assets/js/data-table.js')}}"></script>
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title">
                @if(Auth::user()->role == "people")
                    My complaints
                @else
                    List complaints
                @endif
            </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route(Auth::user()->role . '.dashboard')}}">Dashboard</a></li>
                    <li class="breadcrumb-item active" aria-current="page">
                        @if(Auth::user()->role == "people")
                            My complaints
                        @else
                            List complaints
                        @endif
                    </li>
                </ol>
            </nav>
        </div>
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Data complaints</h4>
                <div class="row">
                    <div class="col-12">
                        <table id="order-listing" class="table">
                            <thead>
                            <tr>
                                <th class="text-center">Order #</th>
                                @if(Auth::user()->role != 'people')
                                <th>Person</th>
                                @endif
                                <th>Title</th>
                                <th class="text-center">Sent at</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($complaints as $i => $complaint)
                                <tr>
                                    <td class="text-center">{{$i+1}}</td>
                                    @if(Auth::user()->role != 'people')
                                    <td>{{$complaint->people->name}}</td>
                                    @endif
                                    <td>{{$complaint->title}}</td>
                                    <td class="text-center">{{$complaint->created_at->diffForHumans()}}</td>
                                    <td class="text-center">
                                        <span class="badge badge-{{getColor($complaint->status)}}">
                                            <i class="mdi mr-1" style="font-size: 11px; vertical-align: middle">
                                                {{getIcon($complaint->status)}}
                                            </i> {{getStatus($complaint->status)}}
                                        </span>
                                    </td>
                                    <td class="text-center">
                                        <a href="{{route(Auth::user()->role . '.complaint.show', ['complaint' => $complaint->id])}}" class="btn btn-info">View</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>


@endsection

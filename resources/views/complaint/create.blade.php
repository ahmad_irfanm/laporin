@extends('layouts.admin')

@section('title')
    Add a complaint
@endsection

@section('css')
    <style>
        iframe {
            width: 100% !important;
        }
    </style>
@endsection

@section('js')
    <script src="{{asset('assets/vendors/jquery-steps/jquery.steps.min.js')}}"></script>
    <script src="{{asset('assets/js/wizard.js')}}"></script>
    <script>
        $('#location').on('input', function (e) {
            let html = $.parseHTML($(this).val());

            if (html.length > 0) {
                html = html[0];

                if (html.nodeName == "IFRAME") {
                    $('#location-embed').html(html);
                } else {
                    $('#location-embed').html(`<span class="text-danger mb-2">Location format is wrong</span>`);
                }
            }
        });
    </script>
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title"> Add a complaint </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route('people.dashboard')}}">Dashboard</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Add a complaint</li>
                </ol>
            </nav>
        </div>
        <!--vertical wizard-->
        <div class="row">
            <div class="col-12 grid-margin">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Form step complaint</h4>

                        @if($errors->any())
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $error)
                                    {{$error}}
                                @endforeach
                            </div>
                        @endif

                        <form id="example-vertical-wizard" action="{{route('people.complaint.store')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div>
                                <h3>Title &amp; Description</h3>
                                <section>
                                    <h3>Title &amp; Description</h3>
                                    <div class="form-group">
                                        <label for="userName">Title *</label>
                                        <input id="userName" name="title" type="text" class="required form-control" value="{{old('title')}}">
                                    </div>
                                    <div class="form-group">
                                        <label for="description">Description *</label>
                                        <textarea name="description" id="description" cols="30" rows="10" class="required form-control">{{old('description')}}</textarea>
                                    </div>
                                </section>
                                <h3>Upload proof</h3>
                                <section>
                                    <h3>Proof File</h3>
                                    <div class="form-group">
                                        <label for="proof">File</label>
                                        <input id="proof" name="proof" type="file" class="required form-control">
                                    </div>
                                </section>
                                <h3>Location</h3>
                                <section>
                                    <h3>Location</h3>
                                    <div class="form-group mb-2">
                                        <label for="location">Embed location url</label>
                                        <textarea name="location" id="location" class="required form-control" cols="30" rows="10" placeholder="Apply iframe tag here"></textarea>
                                    </div>
                                    <div id="location-embed">

                                    </div>
                                    <a href="https://support.google.com/maps/answer/144361?co=GENIE.Platform%3DDesktop&hl=en" target="_blank">How to get embed location in google maps ?</a>
                                </section>
                                <h3>Finish</h3>
                                <section>
                                    <h3>Finish</h3>
                                    <div class="form-check">
                                        <label class="form-check-label">
                                            <input class="checkbox" type="checkbox"> I agree with the Terms and Conditions. </label>
                                    </div>
                                </section>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

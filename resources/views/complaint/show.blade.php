@extends('layouts.admin')

@section('title')
    {{$complaint->title}}
@endsection

@section('complaints')
    active
@endsection

@section('css')
    <style>
        iframe {
            width: 100% !important;
        }
    </style>
@endsection

@section('js')
    <script src="{{asset('assets/vendors/sweetalert/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/js/modal-demo.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('#send').click('click', (e) => {
               e.preventDefault();

               $('#modal-confirm').modal('show');
            });

            $('#confirm').click(function () {
               $('#send').parents('form').submit();
               $(this).html(`
                        <div class="bar-loader" style="height: 8px; display: inline-block;">
                            <span style="background: #fff;"></span>
                            <span style="background: #fff;"></span>
                            <span style="background: #fff;"></span>
                            <span style="background: #fff;"></span>
                        </div>
               `);
            });

            $('#response').click('click', (e) => {
                e.preventDefault();

                $('#modal-response').modal('show');
                setTimeout(() => {
                    $('#response-input').focus();
                }, 600);
            });

            $('#confirm-response').click(function () {
                $(this).parents('form').submit();
                $(this).html(`
                        <div class="bar-loader" style="height: 8px; display: inline-block;">
                            <span style="background: #fff;"></span>
                            <span style="background: #fff;"></span>
                            <span style="background: #fff;"></span>
                            <span style="background: #fff;"></span>
                        </div>
               `);
            });
        })

        @if(session('msg'))
            swal({
                title: 'Successful',
                text: '{{session('msg')}}',
                icon: 'success',
                button: {
                    text: "Continue",
                    value: true,
                    visible: true,
                    className: "btn btn-primary"
                }
            })
        @endif

        @if($errors->any())
            let text = "";
            @foreach($errors->all() as $error)
                text += '{{$error}} ';
            @endforeach
            swal({
                title: 'Something error',
                text: text,
                html: true,
                icon: 'error',
                button: {
                    text: "Continue",
                    value: true,
                    visible: true,
                    className: "btn btn-primary"
                }
            })
        @endif
    </script>
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title"> Detail Complaint </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route(Auth::user()->role . '.dashboard')}}">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="{{route(Auth::user()->role . '.complaint.index')}}">Complaints</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Detail Complaint</li>
                </ol>
            </nav>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">

                            @if(Auth::user()->role != "people")
                            <div class="col-lg-4">
                                <div class="border-bottom text-center pb-4">
                                    <img src="{{URL::asset('storage/app/' . $complaint->people->photo)}}" alt="profile" class="img-lg rounded-circle mb-3" />
{{--                                    <p>{{$complaint->people->address}}</p>--}}
                                </div>
                                <div class="py-4">
                                    <p class="clearfix">
                                        <span class="float-left"> NIK </span>
                                        <span class="float-right text-muted"> {{$complaint->people->nik}} </span>
                                    </p>
                                    <p class="clearfix">
                                        <span class="float-left"> Name </span>
                                        <span class="float-right text-muted"> {{$complaint->people->name}} </span>
                                    </p>
                                    <p class="clearfix">
                                        <span class="float-left"> Phone </span>
                                        <span class="float-right text-muted"> {{$complaint->people->phone}} </span>
                                    </p>
                                    <p class="clearfix">
                                        <span class="float-left"> Username </span>
                                        <span class="float-right text-muted"> {{$complaint->people->user->username}} </span>
                                    </p>
                                </div>
                            </div>
                            @endif
                            <div class="col-lg-{{Auth::user()->role == "people" ? '12' : '8'}}">
                                <div class="d-flex justify-content-between">
                                    <div>
                                        <h4 class="mb-3">{{$complaint->title}}</h4>
                                        <span class="badge badge-{{getColor($complaint->status)}} mr-1">
                                                <i class="mdi mr-1" style="font-size: 11px; vertical-align: middle">{{getIcon($complaint->status)}}</i> {{getStatus($complaint->status)}}
                                        </span>
                                        <span class="badge badge-primary">
                                                <i class="mdi mr-1" style="font-size: 11px; vertical-align: middle">calendar_today</i>
                                                {{$complaint->created_at->diffForHumans()}}
                                        </span>
                                    </div>

                                    @if(Auth::user()->role != "people" && $complaint->status == 2)
                                    <div>
                                        <button class="btn btn-gradient-success d-flex align-items-center" id="response">
                                            <i class="mdi mr-2" style="font-size: 24px;">done_all</i> Response this complaint
                                        </button>
                                    </div>
                                    @elseif (Auth::user()->role == "people" && $complaint->status == 1)
                                        <div>
                                            <form action="{{route('people.complaint.update', ['complaint'=>$complaint->id])}}" method="post">
                                                @method('put')
                                                @csrf
                                                <input type="hidden" name="status" value="2">
                                                <button type="button" class="btn btn-gradient-primary d-flex align-items-center" id="send">
                                                    <i class="mdi mr-2" style="font-size: 24px;">near_me</i> Send this complaint
                                                </button>
                                            </form>
                                        </div>
                                    @elseif($complaint->status == 3)
                                        <div>
                                            <button class="btn btn-gradient-info d-flex align-items-center" data-toggle="modal" data-target="#see-response">
                                                <i class="mdi mr-2" style="font-size: 24px;">assignment</i> See response
                                            </button>
                                        </div>
                                    @endif
                                </div>
                                <div class="profile-feed">
                                    <div class="d-flex align-items-start profile-feed-item">
                                        <div>
                                            <p> {{$complaint->description}} </p>
                                            <img src="{{URL::asset('storage/app/' . $complaint->proof)}}" alt="sample" class="rounded mw-100" />
                                        </div>
                                    </div>
                                </div>
                                @if($complaint->location)
                                    <div class="profile-feed">
                                        <div class="d-flex align-items-start profile-feed-item">
                                            <div style="width: 100%;">
                                                <p class="font-heading">Location</p>
                                                {!! $complaint->location !!}
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Confirm your action</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Do you want to send it ?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-gradient-primary" id="confirm">
                        Yes, send now!
                    </button>
                    <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="see-response" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Response of this complaint</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @if($complaint->response)
                        <img class="d-block img-sm rounded-circle mx-auto mb-2" src="{{URL::asset('storage/app/' . $complaint->response->officer->photo)}}" alt="profile image">
                        <p class="text-center user-name font-heading">{{$complaint->response->officer->name}}</p>
                        <p class="text-center mb-2 comment" style="color: #9c9fa6">
                            <i class="mdi mr-1">format_quote</i>{{$complaint->response->content}}<i class="mdi ml-1">format_quote</i>
                        </p>
                        <small class="d-block mt-4 text-center posted-date" style="color: #b66dff">{{$complaint->response->created_at->diffForHumans()}}</small>
                    @endif
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <form class="modal fade" id="modal-response" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" action="{{route(Auth::user()->role . '.complaint.update', ['complaint' => $complaint->id])}}" method="post">
        @csrf
        @method('put')
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Response this complaint</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="status" value="3">
                    <div class="form-group">
                        <label for="response-input">Response</label>
                        <textarea name="content" id="response-input" class="form-control" cols="30" rows="10"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" id="confirm-response">
                        Response now!
                    </button>
                    <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </form>
@endsection

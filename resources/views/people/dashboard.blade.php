@extends('layouts.admin')

@section('title')
    Dashboard
@endsection

@section('dashboard')
    active
@endsection

@section('css')
@endsection

@section('js')
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title">
                <span class="page-title-icon bg-gradient-primary text-white mr-2">
                  <i class="mdi">home</i>
                </span> Dashboard </h3>
            <nav aria-label="breadcrumb">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item active" aria-current="page">
                        <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="row">
            <div class="col-12 grid-margin">
                <div class="card card-statistics">
                    <div class="row">
                        <div class="card-col col-xl-43 col-lg-4 col-md-4 col-12 border-right">
                            <div class="card-body">
                                <div class="d-flex align-items-center justify-content-center flex-column flex-sm-row">
                                    <i class="mdi text-primary mr-0 mr-sm-4 icon-lg">drafts</i>
                                    <div class="wrapper text-center text-sm-left">
                                        <p class="card-text mb-0">Draft</p>
                                        <div class="fluid-container">
                                            <h3 class="mb-0 font-weight-medium">{{\App\Complaint::where('people_id', Auth::user()->people->id)->whereStatus(1)->count()}}</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-col col-xl-4 col-lg-4 col-md-4 col-12">
                            <div class="card-body">
                                <div class="d-flex align-items-center justify-content-center flex-column flex-sm-row">
                                    <i class="mdi text-primary mr-0 mr-sm-4 icon-lg">near_me</i>
                                    <div class="wrapper text-center text-sm-left">
                                        <p class="card-text mb-0">Sent</p>
                                        <div class="fluid-container">
                                            <h3 class="mb-0 font-weight-medium">{{\App\Complaint::where('people_id', Auth::user()->people->id)->whereStatus(2)->count()}}</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-col col-xl-4 col-lg-4 col-md-4 col-12 border-right">
                            <div class="card-body">
                                <div class="d-flex align-items-center justify-content-center flex-column flex-sm-row">
                                    <i class="mdi text-primary mr-0 mr-sm-4 icon-lg">done_all</i>
                                    <div class="wrapper text-center text-sm-left">
                                        <p class="card-text mb-0">Responded</p>
                                        <div class="fluid-container">
                                            <h3 class="mb-0 font-weight-medium">{{\App\Complaint::where('people_id', Auth::user()->people->id)->whereStatus(3)->count()}}</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 grid-margin stretch-card">
                <div class="card review-card">
                    <div class="card-header header-sm d-flex justify-content-between align-items-center">
                        <h4 class="card-title mb-0">New responded complaints</h4>
                        <div class="wrapper d-flex align-items-center">
                            <div class="dropdown">
                                <button class="btn btn-transparent icon-btn pr-0" type="button" id="dropdownMenuIconButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="mdi">more_vert</i>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton1" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 46px, 0px);">
                                    <a class="dropdown-item" href="{{route('people.complaint.index')}}">See all complaints</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body no-gutter">

                        @foreach(\App\Complaint::where('status', '3')->where('people_id', Auth::user()->people->id)->skip(0)->take(5)->orderBy('created_at', 'desc')->get() as $complaint)
                            <div class="list-item">
                                <div class="preview-image">
                                    <img class="img-sm rounded-circle" src="{{URL::asset('storage/app/' . $complaint->response->officer->photo)}}" alt="profile image">
                                </div>
                                <div class="content">
                                    <div class="d-flex align-items-center">
                                        <h6 class="product-name">{{$complaint->response->officer->name}}</h6>
                                        <small class="time ml-3 d-none d-sm-block">{{$complaint->response->created_at->diffForHumans()}}</small>
                                        <div class="ml-auto">
                                            <a href="{{route('people.complaint.show', ['complaint' => $complaint->id])}}" class="btn btn-primary btn-sm" style="display: inline;">View complaint</a>
                                        </div>
                                    </div>
                                    <div class="d-flex align-items-center">
                                        <p class="user-name">Say :</p>
                                        <p class="review-text d-block">{{$complaint->response->content}}</p>
                                    </div>
                                    <div class="d-flex align-items-center">
                                        <small class="user-name">For :</small>
                                        <small class="review-text d-block" style="color: #9c9fa6;">{{$complaint->title}}</small>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                            @if(\App\Complaint::where('status', '3')->where('people_id', Auth::user()->people->id)->count() == 0)
                                No data
                            @endif
                    </div>
                </div>
            </div>


        </div>
    </div>
@endsection

@extends('layouts.admin')

@section('title')
        Manage Officers
@endsection

@section('officers')
    active
@endsection

@section('css')
    <link rel="stylesheet" href="{{asset('assets/vendors/datatables.net-bs4/dataTables.bootstrap4.css')}}">
@endsection

@section('js')
    <script src="{{asset('assets/vendors/datatables.net/jquery.dataTables.js')}}"></script>
    <script src="{{asset('assets/vendors/datatables.net-bs4/dataTables.bootstrap4.js')}}"></script>
    <script src="{{asset('assets/js/data-table.js')}}"></script>
    <script src="{{asset('assets/vendors/sweetalert/sweetalert.min.js')}}"></script>

    <script>
        $('body').delegate('#edit', 'click',  function (e) {
            e.preventDefault();

            let username = $(this).parents('tr').find('#td-username').text();
            let name = $(this).parents('tr').find('#td-name').text();
            let phone = $(this).parents('tr').find('#td-phone').text();
            let photo = $(this).parents('tr').find('#td-img img').attr('src');

            $('#username').val(username);
            $('#fullname').val(name);
            $('#phone').val(phone);
            $('#preview').attr('src', photo);
            $('#modal-edit').attr('action', $(this).attr('href'));

            $('#modal-edit').modal('show');
        });

        $('body').delegate('#delete', 'click',  function (e) {
            e.preventDefault();

            swal({
                title: 'Are you sure?',
                text: "Do you want to delete it?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3f51b5',
                cancelButtonColor: '#ff4081',
                confirmButtonText: 'Great ',
                buttons: {
                    cancel: {
                        text: "Cancel",
                        value: null,
                        visible: true,
                        className: "btn btn-danger",
                        closeModal: true,
                    },
                    confirm: {
                        text: "OK",
                        value: true,
                        visible: true,
                        className: "btn btn-primary",
                        closeModal: true
                    }
                }
            }).then((result) => {
                if (result) {
                    let token = $('meta[name="csrf-token"]').attr('content');
                    let url = $(this).attr('href');
                    $.ajax({
                        url,
                        method: 'post',
                        data: {_token: token, _method: 'delete'},
                        success: function (res) {
                            swal({
                                title: 'Successful',
                                text: res.message,
                                icon: 'success',
                                button: {
                                    text: "Continue",
                                    value: true,
                                    visible: true,
                                    className: "btn btn-primary"
                                }
                            }).then((result) => {
                               location.reload();
                            });
                        },
                        error: function (res) {
                            swal({
                                title: 'Something error',
                                text: res.responseJSON.message,
                                icon: 'error',
                                button: {
                                    text: "Continue",
                                    value: true,
                                    visible: true,
                                    className: "btn btn-primary"
                                }
                            })
                        }
                    })

                }
            })
        });

        $('#new').click(function (e) {
            e.preventDefault();

            $('#modal-add').modal('show');
        });

        @if(session('msg'))
            swal({
                title: 'Successful',
                text: '{{session('msg')}}',
                icon: 'success',
                button: {
                    text: "Continue",
                    value: true,
                    visible: true,
                    className: "btn btn-primary"
                }
            })
        @endif

        @if($errors->any())
            let text = "";
            @foreach($errors->all() as $i => $error)
                text += '{{$error}} ';
            @endforeach
            swal({
                title: 'Something error',
                text: text,
                html: true,
                icon: 'error',
                button: {
                    text: "Continue",
                    value: true,
                    visible: true,
                    className: "btn btn-primary"
                }
            })
        @endif
    </script>
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title">
                Manage Officers
            </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{route(Auth::user()->role . '.dashboard')}}">Dashboard</a></li>
                    <li class="breadcrumb-item active" aria-current="page">
                        Manage Officers
                    </li>
                </ol>
            </nav>
        </div>
        <button type="button" id="new" class="btn btn-gradient-primary mb-4 d-flex align-items-center"><i class="mdi mr-1">add</i> Add a officer</button>
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Data officers</h4>
                <div class="row">
                    <div class="col-12">
                        <table id="order-listing" class="table">
                            <thead>
                            <tr>
                                <th class="text-center">Order #</th>
                                <th>Username</th>
                                <th>Fullname</th>
                                <th class="text-center">Phone</th>
                                <th class="text-center">Created at</th>
                                <th class="text-center">Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($officers as $i => $officer)
                                <tr>
                                    <td class="text-center">{{$i+1}}</td>
                                    <td id="td-username">{{$officer->user->username}}<div id="td-img" style="display: none;"><img src="{{URL::asset('storage/app/' . $officer->photo)}}" alt=""></div></td>
                                    <td id="td-name">{{$officer->name}}</td>
                                    <td class="text-center" id="td-phone">{{$officer->phone}}</td>
                                    <td class="text-center">{{$officer->created_at->diffForHumans()}}</td>
                                    <td class="text-center">
                                        <a href="{{route('admin.officer.update', ['officer' => $officer->id])}}" id="edit" class="btn btn-warning mr-2">Edit</a>
                                        <a href="{{route('admin.officer.destroy', ['officer' => $officer->id])}}" id="delete" class="btn btn-danger">Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <form class="modal fade" id="modal-add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" action="{{route('admin.officer.store')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add a officer</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="fullname-a">Fullname</label>
                            <input type="text" name="name" id="fullname-a" class="form-control" value="{{old('name')}}">
                        </div>
                        <div class="form-group">
                            <label for="phone-a">Phone</label>
                            <input type="text" name="phone" id="phone-a" class="form-control" value="{{old('phone')}}">
                        </div>
                        <div class="form-group">
                            <label for="photo-a">Photo</label>
                            <input type="file" name="photo" id="photo-a" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="username-a">Username</label>
                            <input type="text" name="username" id="username-a" class="form-control" value="{{old('username')}}">
                        </div>
                        <div class="form-group">
                            <label for="password-a">Password</label>
                            <input type="password" name="password" id="password-a" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="password_confirmation-a">Password Confirmation</label>
                            <input type="password" name="password_confirmation" id="password_confirmation-a" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">
                            Save
                        </button>
                        <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </form>
        <form class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" action="" method="post" enctype="multipart/form-data">
            @csrf
            @method('put')
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit officer</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text" name="username" id="username" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="fullname">Fullname</label>
                            <input type="text" name="name" id="fullname" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="phone">Phone</label>
                            <input type="text" name="phone" id="phone" class="form-control">
                        </div>
                        <div class="form-group">
                            <img src="" id="preview" class="mb-4" style="width: 200px; display: block">
                            <label for="photo">Change Photo</label>
                            <input type="file" name="photo" id="photo" class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">
                            Update
                        </button>
                        <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </form>
@endsection

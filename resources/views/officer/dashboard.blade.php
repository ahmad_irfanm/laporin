@extends('layouts.admin')

@section('title')
    Dashboard
@endsection

@section('dashboard')
    active
@endsection

@section('css')
@endsection

@section('js')
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="page-header">
            <h3 class="page-title">
                <span class="page-title-icon bg-gradient-primary text-white mr-2">
                  <i class="mdi">home</i>
                </span> Dashboard </h3>
            <nav aria-label="breadcrumb">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item active" aria-current="page">
                        <span></span>Overview <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="row">

            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 grid-margin stretch-card">
                <div class="card card-statistics">
                    <div class="card-body">
                        <div class="clearfix">
                            <div class="float-left">
                                <i class="mdi text-danger icon-lg">comment</i>
                            </div>
                            <div class="float-right">
                                <p class="mb-0 text-right">Complaints</p>
                                <div class="fluid-container">
                                    <h3 class="font-weight-medium text-right mb-0">{{\App\Complaint::where('status', '!=', 1)->count()}}</h3>
                                </div>
                            </div>
                        </div>
                        <p class="text-muted mt-3 mb-0">
                            <i class="mdi mdi-alert-octagon mr-1" aria-hidden="true"></i> All of complaints </p>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 grid-margin stretch-card">
                <div class="card card-statistics">
                    <div class="card-body">
                        <div class="clearfix">
                            <div class="float-left">
                                <i class="mdi text-warning icon-lg">inbox</i>
                            </div>
                            <div class="float-right">
                                <p class="mb-0 text-right">Received</p>
                                <div class="fluid-container">
                                    <h3 class="font-weight-medium text-right mb-0">{{\App\Complaint::whereStatus(2)->count()}}</h3>
                                </div>
                            </div>
                        </div>
                        <p class="text-muted mt-3 mb-0">
                            <i class="mdi mdi-bookmark-outline mr-1" aria-hidden="true"></i> complaints are received </p>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 grid-margin stretch-card">
                <div class="card card-statistics">
                    <div class="card-body">
                        <div class="clearfix">
                            <div class="float-left">
                                <i class="mdi text-success icon-lg">done_all</i>
                            </div>
                            <div class="float-right">
                                <p class="mb-0 text-right">Responded</p>
                                <div class="fluid-container">
                                    <h3 class="font-weight-medium text-right mb-0">{{\App\Complaint::whereStatus(3)->count()}}</h3>
                                </div>
                            </div>
                        </div>
                        <p class="text-muted mt-3 mb-0">
                            <i class="mdi mdi-calendar mr-1" aria-hidden="true"></i> complaints are responded </p>
                    </div>
                </div>
            </div>



            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 grid-margin stretch-card">
                <div class="card review-card">
                    <div class="card-header header-sm d-flex justify-content-between align-items-center">
                        <h4 class="card-title mb-0">New received complaints</h4>
                        <div class="wrapper d-flex align-items-center">
                            <div class="dropdown">
                                <button class="btn btn-transparent icon-btn pr-0" type="button" id="dropdownMenuIconButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="mdi">more_vert</i>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton1" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 46px, 0px);">
                                    <a class="dropdown-item" href="{{route('people.complaint.index')}}">Today</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body no-gutter">

                        @foreach(\App\Complaint::where('status', '2')->orderBy('created_at', 'desc')->skip(0)->take(5)->get() as $complaint)
                            <div class="list-item">
                                <div class="preview-image">
                                    <img class="img-sm rounded-circle" src="{{URL::asset('storage/app/' . $complaint->people->photo)}}" alt="profile image">
                                </div>
                                <div class="content">
                                    <div class="d-flex align-items-center">
                                        <h6 class="product-name">{{$complaint->people->name}}</h6>
                                        <small class="time ml-3 d-none d-sm-block">{{$complaint->created_at->diffForHumans()}}</small>
                                        <div class="ml-auto">
                                            <a href="{{route(Auth::user()->role . '.complaint.show', ['complaint' => $complaint->id])}}" class="btn btn-primary btn-sm" style="display: inline;">View complaint</a>
                                        </div>
                                    </div>
                                    <div class="d-flex align-items-center">
                                        <p class="user-name">Say :</p>
                                        <p class="review-text d-block">{{$complaint->title}}</p>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                        @if(\App\Complaint::where('status', '2')->orderBy('created_at', 'desc')->skip(0)->take(5)->count() == 0)
                            No data
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

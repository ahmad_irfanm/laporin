<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Officer;
use App\People;
use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'username' => 'admin',
            'password' => bcrypt('password'),
            'role' => 'admin'
        ]);
        Officer::create([
            'user_id' => $admin->id,
            'name' => 'Administrator',
            'photo' => 'public/officers/default.jpg',
            'phone' => '082184731821'
        ]);

        $officer = User::create([
            'username' => 'officer',
            'password' => bcrypt('password'),
            'role' => 'officer'
        ]);
        Officer::create([
            'user_id' => $officer->id,
            'name' => 'Petugas',
            'photo' => null,
            'phone' => '085928172844'
        ]);

        $faker = Faker::create();

        foreach (range(1, 10) as $item) {
            $user = User::create([
                'username' => 'user' . $item,
                'password' => bcrypt('password'),
                'role' => 'people'
            ]);

            $people = People::create([
                'user_id' => $user->id,
                'name' => $faker->name,
                'nik' => $faker->regexify('[0-9]{15,15}'),
                'address' => $faker->address,
                'photo' => 'public/users/default.jpg',
                'phone' => $faker->phoneNumber
            ]);

            foreach (range(1, 3) as $i) {
                $complaint = \App\Complaint::create([
                    'people_id' => $people->id,
                    'title' => $faker->sentence($nbWords = 3, $variableNbWords = true),
                    'description' => $faker->text($maxNbChars = 200),
                    'proof' => 'public/proofs//9verFjfLCMlEboTPfblYw1byJ0DTAK6jKxwthVU8.jpeg',
                    'status' => 1
                ]);
            }
        }
        // $this->call(UsersTableSeeder::class);
    }
}

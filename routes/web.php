<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (Auth::guest()) {
        return redirect()->route('login');
    } else {
        return redirect()->route(Auth::user()->role . '.dashboard');
    }
});

Auth::routes();

//administrator
Route::group(['prefix' => '/administrator', 'as' => 'admin.', 'middleware' => ['admin']], function () {

    //dashboard
    Route::get('/', 'AdminController@dashboard')->name('dashboard');

    //complaint
    Route::resource('/complaint', 'ComplaintController');

    //people
    Route::resource('/people', 'PeopleController');

    //officer
    Route::resource('/officer', 'OfficerController');

    //report
    Route::get('/report', 'AdminController@report')->name('report');

});

//officer
Route::group(['prefix' => '/officer', 'as' => 'officer.', 'middleware' => ['officer']], function () {

    //dashboard
    Route::get('/', 'OfficerController@dashboard')->name('dashboard');

    //complaint
    Route::resource('/complaint', 'ComplaintController');

});

//people
Route::group(['prefix' => '/people', 'as' => 'people.', 'middleware' => ['people']], function () {

    //dashboard
    Route::get('/', 'PeopleController@dashboard')->name('dashboard');

    //complaint
    Route::resource('/complaint', 'ComplaintController');

});

